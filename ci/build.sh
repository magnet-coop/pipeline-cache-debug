#!/usr/bin/env bash
# shellcheck disable=SC1010

set -eu -o pipefail


docker run \
    --rm \
    --volume "$(pwd)/app:/app" \
    --volume "$(pwd)/m2:/root/.m2" \
    --workdir "/app" \
    clojure:lein-2.8.1 \
    lein do check, uberjar
